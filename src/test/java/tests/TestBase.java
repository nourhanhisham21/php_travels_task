/**
 * Before running the test cases user can choose which browser to use
 * "1" for Chrome
 * "2" for Firefox
 * "3" for IE
 * 
 * check test cases results to take screenshot on failure
 * 
 * quite the browser after finishing the test
 */
package tests;


import java.util.Scanner;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;



import utilities.Helper;

public class TestBase {
	
	public static WebDriver driver;
	
	
	@BeforeSuite
	public void startDriver()
	{
		
		int inputt;

        Scanner input = new Scanner(System.in);

        System.out.println("please write 1 for chrome, 2 for firefox or 3 for internet explorer");
        inputt = input.nextInt();
        input.close();
       
        
        if(inputt==1) 
        {
        	
        	String chromepath=System.getProperty("user.dir")+"\\resources\\chromedriver.exe";
    		System.setProperty("webdriver.chrome.driver",chromepath );
    		
        	driver = new ChromeDriver();
        	
    		
        }
        
        else if(inputt==2) 
        {
        	
        	String FireFoxpath=System.getProperty("user.dir")+"\\resources\\geckodriver.exe";
    		System.setProperty("webdriver.gecko.driver",FireFoxpath );

        	driver = new FirefoxDriver();
        	

        }
        
        else
        {
        	String IEpath=System.getProperty("user.dir")+"\\resources\\IEDriverServer.exe";
    		System.setProperty("webdriver.ie.driver",IEpath );
    		
    		driver=new InternetExplorerDriver();
    		
        	
        }

	}
	
	@AfterSuite
	public void StopDriver() 
{
		driver.quit();
			           
			    
	}
	
	
	
	@AfterMethod
	public void ScreenshotOnFailure(ITestResult result) 
	{
		if(result.getStatus()==ITestResult.FAILURE) 
		{
			System.out.println("Took Screenshot...");
			 Helper.TakeScreenshotOnFailure(driver, result.getName());
		}
	}
	

}
