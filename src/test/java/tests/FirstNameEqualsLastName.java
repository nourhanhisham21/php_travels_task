/**
 * all the page and methods are imported here
 ** 
 * 
 * retrieve url,user data and element locators from properties file
 * Test if Warning appear when user insert first name equals to last name
 */

package tests;

import java.io.IOException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.opencsv.exceptions.CsvValidationException;
import data.ReadProperties;
import pages.RegisterationPage;
import static org.testng.Assert.assertTrue;


public class FirstNameEqualsLastName extends TestBase 
{
	RegisterationPage Page1Object;
	String URL = ReadProperties.userData.getProperty("url");
	String FirstName = ReadProperties.userData.getProperty("FirstNameValue");
	String FirstNameBox =ReadProperties.userData.getProperty("FirstNameBox");
	String LastNameBox= ReadProperties.userData.getProperty("LastNameBox");
	String PhoneBox= ReadProperties.userData.getProperty("PhoneBox");
	String PhoneNumber= ReadProperties.userData.getProperty("PhoneNumber");
	String EmailBox= ReadProperties.userData.getProperty("EmailBox");
	String Email= ReadProperties.userData.getProperty("Email");
	String PasswordBox= ReadProperties.userData.getProperty("PasswordBox");
	String Password= ReadProperties.userData.getProperty("Password");
	String ConfirmPasswordBox= ReadProperties.userData.getProperty("ConfirmPasswordBox");
	String Btn= ReadProperties.userData.getProperty("SignUpBtn");
	String SignUpResultsClass=ReadProperties.userData.getProperty("SignUpResultsClass");
	String FirstNameEqualsLastNameWarning = ReadProperties.userData.getProperty("FirstNameEqualsLastNameWarning");
	
	@Test(priority=1,alwaysRun=true)
	public void SignUpWithFirstNameEqualsLastName() throws InterruptedException, CsvValidationException, IOException 
	{		
		driver.manage().window().maximize();					
		driver.navigate().to(URL);

		
		@SuppressWarnings("deprecation")
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name(FirstNameBox)));



		WebElement firstnameBox=driver.findElement(By.name(FirstNameBox));
		WebElement lastnameBox=driver.findElement(By.name(LastNameBox));
		WebElement phoneBox=driver.findElement(By.name(PhoneBox));
		WebElement emailBox=driver.findElement(By.name(EmailBox));
		WebElement passwordBox =driver.findElement(By.name(PasswordBox));
		WebElement confirmPasswordBox=driver.findElement(By.name(ConfirmPasswordBox));
		WebElement btn=driver.findElement(By.xpath(Btn));
		
		Page1Object = new RegisterationPage(driver);	
		Page1Object.InsertRegistryData(firstnameBox, lastnameBox,phoneBox, emailBox,passwordBox, confirmPasswordBox,FirstName,FirstName,PhoneNumber,Email,Password,Password);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("scrollBy(0,1000)");

		Page1Object.signup(btn);
		
		String actualString = driver.findElement(By.className(SignUpResultsClass)).getText();
		assertTrue(actualString.contains(FirstNameEqualsLastNameWarning));
	}

}




