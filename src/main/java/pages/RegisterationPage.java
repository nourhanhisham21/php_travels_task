/** 
 * this page appears after opening url
 * InsertRegistryData: insert each data to its field
 * SignUp: press on Signup button
 *  
 * */

package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class RegisterationPage extends PageBase{

	public RegisterationPage(WebDriver driver) 
	{
		super(driver);

	}

	

	public void InsertRegistryData(WebElement firstname,WebElement lastname,WebElement mobile, WebElement mail, WebElement pass,WebElement confirm, String name1,String name2, String mobile1, String mail1, String pass1, String pass2)
	{


		firstname.sendKeys(name1);
		lastname.sendKeys(name2);
		mobile.sendKeys(mobile1);
		mail.sendKeys(mail1);
		pass.sendKeys(pass1);
		confirm.sendKeys(pass2);
		
		

	}
	
	public void signup(WebElement Btnn) 
	{
		Btnn.click();
	}


}
