/**
 * all the page and methods are imported here
 ** 
 * 
 * retrieve url,user data and element locators from properties file
 * Test if Warning appear when user insert an email that is not in its valid format
 */

package tests;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.opencsv.exceptions.CsvValidationException;
import data.ReadProperties;
import pages.RegisterationPage;

public class InvalidMail extends TestBase{
	
	
	RegisterationPage Page1Object;
	String URL = ReadProperties.userData.getProperty("url");
	String FirstName = ReadProperties.userData.getProperty("FirstNameValue");
	String FirstNameBox =ReadProperties.userData.getProperty("FirstNameBox");
	String LastName= ReadProperties.userData.getProperty("LastNameValue");
	String LastNameBox= ReadProperties.userData.getProperty("LastNameBox");
	String PhoneBox= ReadProperties.userData.getProperty("PhoneBox");
	String PhoneNumber= ReadProperties.userData.getProperty("PhoneNumber");
	String EmailBox= ReadProperties.userData.getProperty("EmailBox");
	String Email= ReadProperties.userData.getProperty("InvalidEmail");
	String PasswordBox= ReadProperties.userData.getProperty("PasswordBox");
	String Password= ReadProperties.userData.getProperty("Password");
	String ConfirmPasswordBox= ReadProperties.userData.getProperty("ConfirmPasswordBox");
	String Btn= ReadProperties.userData.getProperty("SignUpBtn");
	String InvalidMailWarning= ReadProperties.userData.getProperty("InvalidMailWarning");
	
	@Test(priority=1,alwaysRun=true)
	public void UserInsertInvalidMail() throws InterruptedException, CsvValidationException, IOException 
	{		
		driver.manage().window().maximize();					
		driver.navigate().to(URL);

		
		@SuppressWarnings("deprecation")
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name(FirstNameBox)));



		WebElement firstnameBox=driver.findElement(By.name(FirstNameBox));
		WebElement lastnameBox=driver.findElement(By.name(LastNameBox));
		WebElement phoneBox=driver.findElement(By.name(PhoneBox));
		WebElement emailBox=driver.findElement(By.name(EmailBox));
		WebElement passwordBox =driver.findElement(By.name(PasswordBox));
		WebElement confirmPasswordBox=driver.findElement(By.name(ConfirmPasswordBox));
		WebElement btn=driver.findElement(By.xpath(Btn));
		
		Page1Object = new RegisterationPage(driver);	
		Page1Object.InsertRegistryData(firstnameBox, lastnameBox,phoneBox, emailBox,passwordBox, confirmPasswordBox,FirstName,LastName,PhoneNumber,Email,Password,Password);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("scrollBy(0,1000)");
		

		Page1Object.signup(btn);
		
		WebElement head=driver.findElement(By.id("//header-waypoint-sticky"));
		js.executeScript("arguments[0].scrollIntoView();",head); 
		
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#headersignupform > div.resultsignup > div > p")));
		WebElement warning=driver.findElement(By.cssSelector("#headersignupform > div.resultsignup > div > p"));


		
		assertEquals(warning.getText(), InvalidMailWarning);
		

	}

}

